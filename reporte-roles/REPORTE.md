# Rol Guest 
* Se puede clonar el repositorio.
* Agrega/edita archivos, crea ramas.
* Todos los cambios se quedan locales.
* Permite hacer hasta commit, pero no permite hacer push.
* Permite crear issues.
* Se puede visualizar un registro de actividad del repositorio.
* Se puede visualizar todos los archivos dentro del repositorio.
* No se pude modificar ninguno.

# Rol Reporter
* Se puede clonar el repositorio.
* Crea ramas pero no permite hacer push de la misma.
* Permite modificar y crear archivos pero todo se queda localmente.
* Se puede hacer hasta commit, no permite hacer push.
* Permite crear issues.
* Se puede visualizar todos los archivos del repositorio.
* Se puede visualizar un registro de actividad del repostiorio.
* Permite visualizar las ramas del reositorio.
* Permite ver las solicitudes de merge pero no crearlas.
* Permite ver los miembros del proyecto, con su respectivo rol.
* Se puede comparar entre ramas existentes el contenido de estas.

# Developer
* Se puede clonar el repositorio.
* Crea ramas y ya se puede hacer push de estas.
* Permite crear archivos y subirlos a la nube, pero dentro de una rama creada.
* Permite modificar el contendio de los archivos y subirlos al repositorio, pero dentro de una rama creada.
* No se puede subir modificaciones ni crear nuevos archivos hacia la rama master.
* Permite crear issues.
* Se puede ver un registro de actividad del repositorio.
* Permite ver todas las ramas del repositorio.
* Se puede ver los miembros del repositorio, con su respectivo rol.
* Se visualiza registro de merge request.
* Se puede crear un nuevo merge request, hacia master.
* No permite aceptar las solicitudes de merge request.
* Puedo comparar el contenido entre ramas y crear apartir de esto un nuevo merge request.
* No permite agregas miembros al repositorios, pero si buscarlos los que estan ya agregados.

# Mainteiner
* Todos los permisos habilitados.
* Crea ramas y subirlas al repositorio.
* Crea archivos y subirlos directamente a la rama master.
* Modifica el contenido de los archivos y se puede subir directamente hacia la rama master.
* Se puede ver un registro de actividad del repositorio.
* Permite ver todos los miembros con los respectivos roles de cada uno.
* No se puede modificar el rol de los miembros del repositorio.
* Se puede crear issues.
* Contestar y cerrar issues.
* Agrega nuevos miembros al repostiorio y asignar el rol que se desee.
* Se visualizan un registro de todos los merge request.
* Se puede crear merge request.
* Se puede contestar/aceptar y cerrar merge request.
* Se puede comparar el contenido entre ramas para generar merge request.
* No permite eliminar el proyecto.