# Bibliografía **Carlos Ayala** ![Imagen de Perfil](https://en.gravatar.com/userimage/143389424/4dd8c16624f0a41b8e9406eb098edc72.png)
Hola soy Carlos Ayala,nací el 3 de Octubre de 1995 en Quito, me gusta programar y de dar paseos largos con mis mascotas. Tengo 8 perros los cuales les gustan correr bastante, salimos todos los fines de semana en las mañanas.
Me gusta enfocarme dentro de la programación web y móvil, pienso que son áreas de alto impacto dentro de varias áreas de conocimiento.

## Tabla de contenido
1. [Áreas de interés](#áreas-de-interés)
2. [Música de interés](#música-de-interés)
3. [Contactos](#contactos)

## Áreas de interés
Las áreas de interés que presento son las siguientes:

* Aplicaciones móviles
   
   ![aplicaciones-moviles](http://azpe.es/wp-content/uploads/2017/09/app-mobiel.png)
* Aplicaciones web
   
   ![aplicaciones-web](https://internectia.com/images/uploads/2015/03/20/medium/aplicaciones_web.jpg)
* Redes e infraestructura TI
   
   ![redes-TI](http://bmgroupdevelopers.com/img/misc/redes-tic.png)
* Deportes
   
   ![deportes](https://png.clipart.me/istock/previews/5325/53250726-four-favourite-sports-balls-objects.jpg)

[Volver a tabla de contenido](#tabla-de-contenido)


## Música de interés
* [Flaquita](https://www.youtube.com/watch?v=EyxacgNyV4Q)
* [Musica ligera](https://www.youtube.com/watch?v=IibXYWSBpZw)

[Volver a tabla de contenido](#tabla-de-contenido)

## Contactos

* **Email:** [carlos.ayala01@epn.edu.ec](https://login.microsoftonline.com/common/oauth2/authorize?client_id=4345a7b9-9a63-4910-a426-35363201d503&response_mode=form_post&response_type=code+id_token&scope=openid+profile&state=OpenIdConnect.AuthenticationProperties%3duxNwJwB3qNp0sl0XWW0IDDTGZd1xyYaRvIUXhz1ZOslFRXVdb2NkKy_-Aw7PfjplLX7VHPRlBZmCGJri7RQt8Dm6TKWu1rSvLAbrNTkeelQE7Q6KBNOKBaHUbOLjNTPe&nonce=636700584748551317.NDRjYzkxMDUtMGFiZC00MmI2LTk1NjMtZDgxNDY1MzAzMWViM2UzZTk4YjAtYmVmZi00N2EyLWFlMGQtNmFkMmFiNjk1MDJj&redirect_uri=https%3a%2f%2fwww.office.com%2f&ui_locales=en-US&mkt=en-US&client-request-id=125dd264-16f0-4dbb-af34-8c5d8f38ac39) / [k_arlos210@hotmail.com](https://login.live.com/login.srf?wa=wsignin1.0&rpsnv=13&ct=1534461631&rver=6.7.6640.0&wp=MBI_SSL&wreply=https%3a%2f%2foutlook.live.com%2fowa%2f%3fnlp%3d1%26RpsCsrfState%3d6bd7d762-011d-3d49-18c5-0ff4a9fd93a3&id=292841&CBCXT=out&lw=1&fl=dob%2cflname%2cwld&cobrandid=90015)
* **Teléfono:** 02 2959 722
* **Celular:** 0983281723

[Volver a tabla de contenido](#tabla-de-contenido)